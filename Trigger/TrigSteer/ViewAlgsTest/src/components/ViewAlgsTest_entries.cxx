


#include "../TestFEXAlg.h"
#include "../TestFEXAlgView.h"
#include "../TestMergeAlg.h"
#include "../TestSplitAlg.h"
#include "../MergeRoIsAlg.h"
#include "../TestViewDriver.h"
#include "../TestViewMerger.h"
#include "../SchedulerProxyAlg.h"
#include "../TestCombiner.h"


DECLARE_COMPONENT( TestFEXAlg )
DECLARE_COMPONENT( AthViews::TestFEXAlgView )
DECLARE_COMPONENT( TestMergeAlg )
DECLARE_COMPONENT( TestSplitAlg )
DECLARE_COMPONENT( MergeRoIsAlg )
DECLARE_COMPONENT( TestViewDriver )
DECLARE_COMPONENT( TestViewMerger )
DECLARE_COMPONENT( SchedulerProxyAlg )
DECLARE_COMPONENT( TestCombiner )

